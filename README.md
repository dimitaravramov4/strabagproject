# STRABAG Project

## CSV/XLSX Import Application

This Spring Boot application provides a RESTful API for importing CSV/XLSX files containing sections.
The application uses Spring Web for handling REST requests, Spring Data JPA for database interactions.
The project is implemented following the Model-View-Controller (MVC) architecture.

## Usage
- Clone the repository: git clone https://gitlab.com/dimitaravramov4/strabagproject.git
- Configure the application properties in application.properties
- Use the provided REST API to import CSV/XLSX files with sections.

## API Documentation
1. Add New User
   - Endpoint: /auth/user
   - Method: POST
   - Request Body: UserDTO - JSON object containing user details.
2. Authenticate and Get Token
   - Endpoint: /auth/token
   - Method: POST
   - Request Body: AuthRequest - JSON object containing username and password.
3. Register Job
   - Endpoint: /api/csv
   - Method: POST
   - Request Parameter: file - Multipart file containing CSV/XLSX data.
   - Authorization: Requires the 'USER' role.
   - Response: Asynchronous operation. Returns the job ID for tracking progress.
4. Get File Sections by Job ID
    - Endpoint: /api/csv/{jobId}
    - Method: GET
   - Path Variable: jobId - ID of the job for which to retrieve file sections.
   - Authorization: Requires the 'USER' role.
   - Response: Details of the file sections associated with the specified job ID.
5. Search File Sections by Name and Code
   - Endpoint: /api/csv
   - Method: GET
   - Request Parameters: name - Name of the file section, code - Code of the file section.
   - Authorization: Requires the 'USER' role.
   - Response: Details of the file sections matching the provided name and code.
6. Generate and Export Chart
   - Endpoint: /api/chart/generate
   - Method: GET
   - Authorization: Requires the 'MODERATOR' role.
   - Response: No content. Generates a chart and exports it to a PDF file named "chart.pdf".

## Contributors

Dimitar Avramov
