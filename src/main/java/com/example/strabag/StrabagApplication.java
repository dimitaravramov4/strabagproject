package com.example.strabag;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@SpringBootApplication
public class StrabagApplication {

    public static void main(String[] args) {
        SpringApplication.run(StrabagApplication.class, args);
    }

}
