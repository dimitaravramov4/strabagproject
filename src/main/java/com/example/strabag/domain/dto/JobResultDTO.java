package com.example.strabag.domain.dto;

import com.example.strabag.domain.Section;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class JobResultDTO {
    private List<Section> sections;
}
