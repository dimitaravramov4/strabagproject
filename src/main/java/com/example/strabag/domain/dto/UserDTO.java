package com.example.strabag.domain.dto;

import com.example.strabag.domain.UserRole;
import lombok.Data;
import java.util.Set;

@Data
public class UserDTO
{
    private String id;
    private String username;
    private String firstName;
    private String lastName;
    private String password;
    private String email;
    private boolean isAccountNonExpired;
    private boolean isAccountNonLocked;
    private boolean isCredentialsNonExpired;
    private boolean isEnabled;
    private Set<UserRole> authorities;
}
