package com.example.strabag.repository;

import com.example.strabag.domain.Job;
import com.example.strabag.domain.Section;
import org.springframework.data.jpa.repository.JpaRepository;
public interface JobRepository extends JpaRepository<Job, Long> {
}
