package com.example.strabag.repository;

import com.example.strabag.domain.Section;
import org.jfree.data.category.CategoryDataset;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SectionRepository extends JpaRepository<Section, Long> {
    List<Section> findByGeologicalClassesCode(String code);
    List<Section> findByGeologicalClassesName(String name);
    List<Section> findByGeologicalClassesCodeAndName(String code, String name);
    @Query("SELECT s.name, s.geologicalClasses FROM Section s")
    CategoryDataset getChartData();
}
