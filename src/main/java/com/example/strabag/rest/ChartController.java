package com.example.strabag.rest;

import com.example.strabag.service.ChartService;
import com.example.strabag.service.PdfService;
import lombok.extern.slf4j.Slf4j;
import org.jfree.chart.JFreeChart;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/chart")
public class ChartController {
    private final ChartService chartService;
    private final PdfService pdfService;

    public ChartController(ChartService chartService, PdfService pdfService) {
        this.chartService = chartService;
        this.pdfService = pdfService;
    }

    @GetMapping("/generate")
    @PreAuthorize("hasAuthority('MODERATOR')")
    public void generateAndExportChart() {
        log.info("Generate and export chart");
        JFreeChart chart = chartService.generateChart();
        pdfService.exportChartToPdf(chart, "chart.pdf");
    }
}