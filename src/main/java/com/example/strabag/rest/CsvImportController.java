package com.example.strabag.rest;

import com.example.strabag.domain.dto.JobResultDTO;
import com.example.strabag.service.CsvImportService;
import com.example.strabag.service.exception.InvalidFileTypeException;
import com.opencsv.exceptions.CsvException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.concurrent.CompletableFuture;

@Slf4j
@RestController
@RequestMapping("/api/csv")
public class CsvImportController {
    private static final String CSV_EXTENSION = "csv";
    private static final String XLSX_EXTENSION = "xlsx";
    private final CsvImportService csvImportService;

    public CsvImportController(CsvImportService csvImportService) {
        this.csvImportService = csvImportService;
    }

    @PostMapping
    @PreAuthorize("hasAuthority('USER')")
    public CompletableFuture<Long> registerJob(@RequestParam("file") MultipartFile file) throws CsvException {
        if (!isValidFileType(file)) {
            throw new InvalidFileTypeException("Invalid file type. Supported types are CSV and XLSX.");
        }
        log.info("Register job for processing file: {}", file.getOriginalFilename());
        return csvImportService.registerJob(file);
    }

    @GetMapping("/{jobId}")
    @PreAuthorize("hasAuthority('USER')")
    public JobResultDTO getFileByJobId(@PathVariable Long jobId) {
        log.info("Get file sections by jobId: {}", jobId);
        return csvImportService.getFileByJobId(jobId);
    }

    @GetMapping
    @PreAuthorize("hasAuthority('USER')")
    public JobResultDTO searchFileByNameCode(@RequestParam(required = false) String name,
                                             @RequestParam(required = false) String code) {
        log.info("Search file sections by name: {}, code: {}", name, code);
        return csvImportService.searchFileByNameCode(name, code);
    }

    private boolean isValidFileType(MultipartFile file) {
        String originalFilename = file.getOriginalFilename();
        if (originalFilename != null) {
            String extension = originalFilename.substring(originalFilename.lastIndexOf(".") + 1).toLowerCase();
            return extension.equals(CSV_EXTENSION) || extension.equals(XLSX_EXTENSION);
        }
        return false;
    }
}
