package com.example.strabag.service;

import com.example.strabag.repository.SectionRepository;
import lombok.RequiredArgsConstructor;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.CategoryDataset;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class ChartService {

    private final SectionRepository sectionRepository;

    public JFreeChart generateChart() {
        CategoryDataset dataset = sectionRepository.getChartData();

        JFreeChart chart = ChartFactory.createBarChart(
                "Section Geological Classes ",
                "Section name",
                "Geological Classes",
                dataset
        );

        return chart;
    }
}