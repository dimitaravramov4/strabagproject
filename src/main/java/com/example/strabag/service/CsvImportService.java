package com.example.strabag.service;

import com.example.strabag.domain.Job;
import com.example.strabag.domain.Section;
import com.example.strabag.domain.dto.JobResultDTO;
import com.example.strabag.repository.JobRepository;
import com.example.strabag.repository.SectionRepository;
import com.example.strabag.service.utils.CsvParser;
import com.opencsv.exceptions.CsvException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;

@Slf4j
@Getter
@Setter
@RequiredArgsConstructor
@Service
public class CsvImportService {
    private final SectionRepository sectionRepository;
    private final JobRepository jobRepository;

    @Async
    @Transactional
    public CompletableFuture<Long> registerJob(MultipartFile file) throws CsvException {
        try {
            log.info("Async processing started for file: {}", file.getOriginalFilename());
            List<Section> sections = CsvParser.parseCsv(file);
            List<Section> savedSections = sectionRepository.saveAll(sections);

            Job job = new Job();
            job.setFilename(file.getOriginalFilename());
            job.setSections(savedSections);
            Job savedJob = jobRepository.save(job);

            log.info("Async processing completed for file: {}", file.getOriginalFilename());
            return CompletableFuture.completedFuture(savedJob.getId());
        } catch (IOException e) {
            log.error("Error importing CSV file: " + e.getMessage(), e);
            throw new CompletionException(e);
        }
    }

    @Transactional(readOnly = true)
    public JobResultDTO getFileByJobId(Long jobId) {
        return jobRepository.findById(jobId)
                .map(job -> new JobResultDTO(job.getSections()))
                .orElse(null);
    }

    @Transactional(readOnly = true)
    public JobResultDTO searchFileByNameCode(String name, String code) {
        List<Section> sections;
        if (name != null && code != null) {
             sections = sectionRepository.findByGeologicalClassesCodeAndName(name, code);
        } else if (name != null) {
            sections = sectionRepository.findByGeologicalClassesName(name);
        } else if (code != null) {
            sections = sectionRepository.findByGeologicalClassesCode(code);
        } else {
            sections = sectionRepository.findAll();
        }
        return new JobResultDTO(sections);
    }
}