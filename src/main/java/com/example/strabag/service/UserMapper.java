package com.example.strabag.service;

import com.example.strabag.domain.User;
import com.example.strabag.domain.dto.UserDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {
    User mapUser(UserDTO userDTO);
}