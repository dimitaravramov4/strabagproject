package com.example.strabag.service;

import com.example.strabag.domain.User;
import com.example.strabag.domain.UserRole;
import com.example.strabag.domain.dto.UserDTO;
import com.example.strabag.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder encoder;
    @Autowired
    private UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
    {
        User foundUser = this.userRepository
                .findByUsername(username)
                .orElse(null);

        if (foundUser == null) {
            throw new UsernameNotFoundException("User not found.");
        }

        return foundUser;
    }

    public void addUser(UserDTO userDTO) {
        userDTO.setPassword(encoder.encode(userDTO.getPassword()));
        Set<UserRole> authorities = new HashSet<>();
        if(this.userRepository.findAll().isEmpty()) {
            authorities.add(new UserRole("MODERATOR"));
            authorities.add(new UserRole("USER"));
        } else {
            authorities.add(new UserRole("USER"));
        }
        userDTO.setAuthorities(authorities);

        User user = userMapper.mapUser(userDTO);
        userRepository.save(user);
    }
}
