package com.example.strabag.service.utils;

import com.example.strabag.domain.GeologicalClass;
import com.example.strabag.domain.Section;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import io.micrometer.common.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public class CsvParser {
    public static List<Section> parseCsv(MultipartFile file) throws IOException, CsvException {
        try (Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
            CSVReader csvReader = new CSVReader(reader);
            List<String[]> records = csvReader.readAll();

            return convertToSections(records);
        }
    }

    private static List<Section> convertToSections(List<String[]> records) {
        List<Section> sections = new ArrayList<>();

        for (String[] record : records) {
            Section section = new Section();
            section.setName(record[0]);

            List<GeologicalClass> geologicalClasses = new ArrayList<>();
            for (int i = 1; i < record.length; i += 2) {
                if (StringUtils.isEmpty(record[i])) {
                    continue;
                }
                GeologicalClass geologicalClass = new GeologicalClass();
                geologicalClass.setName(record[i]);
                geologicalClass.setCode(record[i + 1]);
                geologicalClasses.add(geologicalClass);
            }

            section.setGeologicalClasses(geologicalClasses);
            sections.add(section);
        }

        return sections;
    }
}
