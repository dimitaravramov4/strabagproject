package com.example.strabag;

import com.example.strabag.domain.dto.JobResultDTO;
import com.example.strabag.rest.CsvImportController;
import com.example.strabag.service.CsvImportService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.CompletableFuture;

@WebMvcTest(CsvImportController.class)
public class CsvImportControllerTest {

    @MockBean
    private CsvImportService csvImportService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testRegisterJob() throws Exception {
        MockMultipartFile file = new MockMultipartFile("file", "test.csv", MediaType.TEXT_PLAIN_VALUE,
                "Test content".getBytes(StandardCharsets.UTF_8));

        CompletableFuture<Long> mockResult = CompletableFuture.completedFuture(1L);
        Mockito.when(csvImportService.registerJob(file)).thenReturn(mockResult);

        mockMvc.perform(MockMvcRequestBuilders.multipart("/api/csv")
                        .file(file)
                        .contentType(MediaType.MULTIPART_FORM_DATA))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testGetFileByJobId() throws Exception {
        Long jobId = 1L;

        JobResultDTO mockResult = new JobResultDTO();
        Mockito.when(csvImportService.getFileByJobId(jobId)).thenReturn(mockResult);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/csv/{jobId}", jobId))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{}"));
    }

    @Test
    public void testSearchFileByNameCode() throws Exception {
        JobResultDTO mockResult = new JobResultDTO();
        Mockito.when(csvImportService.searchFileByNameCode(Mockito.any(), Mockito.any())).thenReturn(mockResult);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/csv")
                        .param("name", "testName")
                        .param("code", "testCode"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{}"));
    }

    @Test
    public void testInvalidFileTypeException() throws Exception {
        MockMultipartFile file = new MockMultipartFile("file", "test.txt", MediaType.TEXT_PLAIN_VALUE,
                "Test content".getBytes(StandardCharsets.UTF_8));

        mockMvc.perform(MockMvcRequestBuilders.multipart("/api/csv")
                        .file(file)
                        .contentType(MediaType.MULTIPART_FORM_DATA))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
}
