package com.example.strabag;

import com.example.strabag.domain.Job;
import com.example.strabag.domain.Section;
import com.example.strabag.domain.dto.JobResultDTO;
import com.example.strabag.repository.JobRepository;
import com.example.strabag.repository.SectionRepository;
import com.example.strabag.service.CsvImportService;
import com.opencsv.exceptions.CsvException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CsvImportServiceTest {

    @Mock
    private SectionRepository sectionRepository;

    @Mock
    private JobRepository jobRepository;

    @InjectMocks
    private CsvImportService csvImportService;

    @Test
    public void testRegisterJob() throws IOException, CsvException {
        MockMultipartFile file = new MockMultipartFile("file", "test.csv", "text/csv", "Test content".getBytes());

        List<Section> mockSections = new ArrayList<>();
        when(sectionRepository.saveAll(any())).thenReturn(mockSections);

        Job mockJob = new Job();
        mockJob.setId(1L);
        when(jobRepository.save(any())).thenReturn(mockJob);

        CompletableFuture<Long> result = csvImportService.registerJob(file);

        assertEquals(1L, result.join().longValue());

        Mockito.verify(sectionRepository, Mockito.times(1)).saveAll(any());
        Mockito.verify(jobRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void testGetFileByJobId() {
        Job mockJob = new Job();
        mockJob.setId(1L);
        mockJob.setSections(new ArrayList<>());

        when(jobRepository.findById(1L)).thenReturn(Optional.of(mockJob));

        JobResultDTO result = csvImportService.getFileByJobId(1L);

        assertEquals(mockJob.getSections(), result.getSections());
    }

    @Test
    public void testSearchFileByNameCode() {
        String name = "Test Name";
        String code = "Test Code";

        List<Section> mockSections = new ArrayList<>();
        when(sectionRepository.findByGeologicalClassesCodeAndName(name, code)).thenReturn(mockSections);

        JobResultDTO result = csvImportService.searchFileByNameCode(name, code);

        assertEquals(mockSections, result.getSections());
    }
}
