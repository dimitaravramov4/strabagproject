package com.example.strabag;

import com.example.strabag.domain.Section;
import com.example.strabag.service.utils.CsvParser;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
@ExtendWith(MockitoExtension.class)
public class CsvParserTest {
    @Mock
    private CSVReader csvReader;

    @InjectMocks
    private CsvParser csvParser;

    @Test
    public void testParseCsv() throws IOException, CsvException {
        String csvContent =
                "Section1,GeoClass1,GC1,GeoClass2,GC2\n" +
                "Section2,GeoClass2,GC2\n" +
                "Section3,GeoClass5,GCX7";

        InputStream inputStream = new ByteArrayInputStream(csvContent.getBytes());
        MockMultipartFile file = new MockMultipartFile("file", "test.csv", "text/csv", inputStream);

        List<Section> result = csvParser.parseCsv(file);

        assertEquals(3, result.size());
        assertEquals("Section1", result.get(0).getName());
        assertEquals("GeoClass1", result.get(0).getGeologicalClasses().get(0).getName());
        assertEquals("GC1", result.get(0).getGeologicalClasses().get(0).getCode());
        assertEquals("GeoClass2", result.get(0).getGeologicalClasses().get(1).getName());
        assertEquals("GC2", result.get(0).getGeologicalClasses().get(1).getCode());
    }
}
